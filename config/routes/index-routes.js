const express = require('express');
const router = express.Router();
// All new Routes Declaration
const employeesRoutes = require('./employees-routes');
const authRoutes = require('./auth-routes')



router.get('/', (req, res, next) => res.render('index'));
router.use('/api/v1/auth', authRoutes)
router.use('/api/v1/employees', employeesRoutes);

module.exports = router