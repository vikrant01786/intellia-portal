const express = require('express');
const router = express.Router();
const { verifyToken } = require('../../utils/authorization')
const { fileUpload, getEmployeeByDOB, getNewDOJEmployees, getMonthlyHolidays, getDeparmentMembers } = require('../../api/controllers/employeeController');

router.get('/getEmployeeByDOB', verifyToken, getEmployeeByDOB);
router.get('/getNewDOJEmployees', verifyToken, getNewDOJEmployees);
router.get('/getMonthlyHolidays', verifyToken, getMonthlyHolidays);
router.post('/file-upload', verifyToken, fileUpload);
router.get('/getDepartmentMembers', verifyToken, getDeparmentMembers)




module.exports = router;
