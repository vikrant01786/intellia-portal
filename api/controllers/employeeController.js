const Employee = require('../models/employees-model');
const catchAsync = require('../../utils/catchAsync');
const AppError = require('../../utils/appError');
const path = require("path");
const util = require("util");


module.exports = {
    fileUpload: catchAsync(async (req, res, next) => {
        var sendUrl = []
        if (req.files) {
            let file = req.files.upfile;
            if (Array.isArray(file)) {
                for (var i = 0; i < file.length; i++) {
                    let fileName = file[i].name;
                    let extension = path.extname(fileName);
                    let md5 = file[i].md5;
                    Url = "/uploads/employee-file-uploads/" + md5 + extension;
                    sendUrl.push(Url)
                    await util.promisify(file[i].mv)("./public" + Url);
                }
            } else {
                let fileName = file.name;
                let extension = path.extname(fileName);
                let md5 = file.md5;
                Url = "/uploads/employee-file-uploads/" + md5 + extension;
                sendUrl.push(Url)
                await util.promisify(file.mv)("./public" + Url);
            }
        }
        res.json({
            success: 1,
            message: "File Uploaded Successfully",
            Url: sendUrl,
        });
    }),

    getEmployeeByDOB: catchAsync(async (req, res, next) => {
        const result = await Employee.aggregate([
            {
                $match: {
                    $expr: {
                        $eq: [{ $month: '$dob' }, { $month: new Date() }],
                    },
                }
            }
        ])
        res.status(200).json({
            status: 'Success',
            count: result.length,
            dobMembersData: result
        })
    }),

    getNewDOJEmployees: catchAsync(async (req, res, next) => {
        let fromDate = new Date(Date.now() + 60 * 60 * 24 * 30 * 1000);
        var currentDate = new Date(Date.now());
        const result = await Employee.find({
            doj: {
                $gte: new Date(currentDate),
                $lt: new Date(fromDate)
            }

        });

        res.status(200).json({
            status: 'Success',
            count: result.length,
            dojEmployeeData: result
        })
    }),

    getMonthlyHolidays: catchAsync(async (req, res, next) => {

    }),

    getDeparmentMembers: catchAsync(async (req, res, next) => {
        const result = await Employee.find({}, { _id: 1, firstName: 1, lastName: 1, employeeCode: 1, contactNumber: 1, avatar: 1 });

        res.status(200).json({
            status: 'Success',
            count: result.length,
            deptMembersData: result
        })
    }),

}